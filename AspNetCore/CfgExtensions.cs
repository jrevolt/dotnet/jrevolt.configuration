﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace JRevolt.Configuration.AspNetCore
{
   public static class CfgExtensions
   {
      public const string UrlsPath = "urls_path";

      public static ConfigurationLoader CreateDefaultConfigurationLoader<T>() =>
         ConfigurationLoader.Create<T>().UseDefaults();

      public static WebApplicationBuilder CreateDefaultWebApplicationBuilder(IConfiguration cfg)
      {
         var contentRoot = cfg["contentRoot"] ?? ".";
         var builder = WebApplication.CreateBuilder(new WebApplicationOptions
         {
            Args = Environment.GetCommandLineArgs(),
            EnvironmentName = cfg.GetConfigurationProperty(Option.Environment),
            ContentRootPath = contentRoot,
         });
         builder.Configuration.AddConfiguration(cfg);
         builder.WebHost.UseSetting("contentRoot", contentRoot);
         builder.WebHost.UseConfiguredEndpoint(builder.Configuration);
         return builder;
      }

      public static void ConfigureAppConfiguration(this IConfiguration cfg, HostBuilderContext _, IConfigurationBuilder cb)
      {
         var keep = cb.Sources.FirstOrDefault(x => x is ChainedConfigurationSource);

         cb.Sources.Clear();
         if (keep != null) cb.Add(keep);

         // cb.UseSetting() seem to try to write into all cfg sources, which not only does not make sense
         // but also, some of the layers are not writable
         cb.Add(new WriteIgnoringConfigurationSource(cfg));


         cb.Add(new MemoryConfigurationSource()); // writable layer on top
      }

      public static ConfigurationLoader UseDefaults(this ConfigurationLoader loader)
      {
         return loader.UseDefault(CfgOption.EnvironmentPrefix, "ASPNETCORE_,X_");
         // don't set `urls` defaults here, it breaks IIS integration
         // see UseConfiguredEndpoint()
      }

      [ExcludeFromCodeCoverage]
      public static IWebHostBuilder LoadConfiguration<T>(this IWebHostBuilder wbuilder) => wbuilder.LoadConfiguration(typeof(T).Assembly);

      [ExcludeFromCodeCoverage]
      public static IWebHostBuilder LoadConfiguration(this IWebHostBuilder wbuilder, Assembly assembly) =>
         throw new ConfigurationException(
            "Unsupported/broken in net6+. Use:\n" +
            "- CfgExtensions.UseDefaults() before load\n" +
            "- builder.Host.ConfigureAppConfiguration(cfg.ConfigureAppConfiguration) to apply");

      public static string GetUrls(IConfiguration cfg)
         => Regex.Replace(cfg.GetConfigurationProperty(Option.Urls) ?? "", "^([^:]+://[^:]+:[0-9]+).*", "$1");

      public static string GetPathBase(IConfiguration cfg)
         => cfg[UrlsPath] ??
            Regex.Replace(cfg.GetConfigurationProperty(Option.Urls) ?? "", "^[^:]+://[^:]+:[0-9]+(.*);?.*", "$1");

      public static IWebHostBuilder UseConfiguredEndpoint(this IWebHostBuilder builder, IConfiguration cfg, bool? iis = null)
      {
         // this flag is leftover from previous call to builder.UseIISIntegration()
         var useIISIntegration = builder.GetSetting("UseIISIntegration")?.ToLower().Equals("true") ?? false;
         if (useIISIntegration)
         {
            Log.Information("IIS configuration detected");
            return builder;
         }

         var urls = GetUrls(cfg);
         var path = GetPathBase(cfg);
         if (string.IsNullOrEmpty(urls)) urls = "http://+:5000";
         builder.UseUrls(urls);
         builder.UseSetting(UrlsPath, path);

         var log = Log.Logger.ForContext(typeof(CfgExtensions));
         log.Information("Configured endpoint: {0}{1}", urls, path);

         return builder;
      }

      public static IApplicationBuilder UseConfiguration(this IApplicationBuilder application)
      {
         return application.UsePathBaseRoot();
      }
   }
}
