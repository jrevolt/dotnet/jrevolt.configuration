using System;
using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Configuration.AspNetCore
{

   [SuppressMessage("ReSharper", "InconsistentNaming")]
   [SuppressMessage("sonar", "S2342:naming")]
   public enum IISOption
   {
      PORT,
      APPL_PATH,
      TOKEN,
   }

   public static class AspnetcoreCfgExtensions
   {
      public static string PropertyName(this IISOption option) => option.ToString();

      public static string EnvironmentVariableName(this IISOption option) => $"ASPNETCORE_{option.ToString().ToUpper()}";

      public static string? EnvironmentVariableValue(this IISOption option) => Environment.GetEnvironmentVariable(option.EnvironmentVariableName());

      public static string? GetConfigurationProperty(this IConfiguration cfg, IISOption option) => cfg[option.PropertyName()];
   }

}
