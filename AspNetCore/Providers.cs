using System.Collections.Generic;
using System.Linq.Expressions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace JRevolt.Configuration.AspNetCore
{
   public class WriteIgnoringConfigurationSource : IConfigurationSource, IConfigurationProvider
   {
      private readonly IConfigurationProvider provider;

      public WriteIgnoringConfigurationSource(IConfiguration cfg)
         => provider = new ChainedConfigurationProvider(new ChainedConfigurationSource{Configuration = cfg});

      public IConfigurationProvider Build(IConfigurationBuilder builder)
         => this;

      public IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath)
         => provider.GetChildKeys(earlierKeys, parentPath);

      public IChangeToken GetReloadToken()
         => provider.GetReloadToken();

      public void Load()
         => provider.Load();

      public void Set(string key, string value)
         => Expression.Empty();

      public bool TryGet(string key, out string value)
         => provider.TryGet(key, out value);

   }
}
