using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using static JRevolt.Configuration.AspNetCore.CfgExtensions;

namespace JRevolt.Configuration.AspNetCore
{
   public static class WebExtensions
   {
      /// <summary>
      /// Enforces single context path for application.
      /// This is alternative to UsePathBase() which only adds alternative mappings but does not really move anything
      /// </summary>
      /// <param name="builder"></param>
      /// <param name="root"></param>
      /// <returns></returns>
      public static IApplicationBuilder UsePathBaseRoot(this IApplicationBuilder builder)
      {
         var cfg = builder.ApplicationServices.GetService<IConfiguration>();
         var root = cfg[UrlsPath];
         if (string.IsNullOrEmpty(root) || root == "/") return builder;

         return builder.Use((ctx, next) =>
         {
            var r = ctx.Request;
            var pathbase = new PathString(root.TrimEnd('/'));

            // this never happens in default config but it might if one also configures UsePathBase()
            if (r.PathBase.Equals(pathbase)) return next();

            // match?
            if (r.PathBase.Value.Equals("") && r.Path.StartsWithSegments(pathbase))
            {
               r.PathBase = pathbase;
               r.Path = r.Path.Value.Replace(pathbase.Value, "");
               return next();
            }

            // anything else: abort and force "not found" status
            ctx.Response.StatusCode = StatusCodes.Status404NotFound;
            return Task.CompletedTask;

            // note: with UsePathBase(), empty pathbase would still match
            // also, multiple UsePathBase() calls end up supporting multiple mappings, e.g.:
            // with UsePathBase("/foo").UsePathBase("/bar"), following /api/version calls would match:
            // - /api/version
            // - /foo/api/version
            // - /bar/api/version
            // this may cause confusion and is the main reason for the introduction of this solution
         });
      }
   }
}
