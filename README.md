
# Features

- includes
- feature profiles
- environment profiles
- multi-prefix system environment variables
- command line

## Configuration Directories

- embedded resources
- code base (path to binaries)
- configurable application directory (e.g. ~/.AssemblyName)
- working directory
- system environment
- command line

## Configuration Files

- `appsettings.yaml`
- `appsettings.${EnvironmentName}.yaml` 
- `appsettings.${Include}.yaml` (0..N files)
- `appsettings.${Profile}.yaml` (0..N files)
- `appsettings.local.yaml`

## Environment Profiles

Property: `Environment`

## Includes

Used to split bloated single file configuration into multiple manageable pieces. 

Property: `Includes`
Type: array and/or comma separated list

e.g.:

```
Includes:
 - Part1
 - Part2 
```

or

`Includes: [ Part1, Part2 ]`

or

`Includes: Part1,Part2`

Typical use cases: logging, database configuration, application administration, etc

## Profiles

Used to enable optional application features or reconfigure defaults for a specific use case.

Property: `Profiles`
Type: array and/or comma separated list (see also `Includes`)

Typical use cases: enable application administration features, enable additional security,  customize logging, etc 

## System Environment

Property: `EnvironmentPrefix`
Type: array and/or comma separated list

## Command Line

See [Command-line configuration provider](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.2#command-line-configuration-provider)

## appsettings.local.yaml

Used for ad-hoc customization of local application deployment. 

Typical use cases: enable specific environment profile or provide missing deployment-specific configuration details, e.g:

```
Environment: Production
ConnectionStrings:
  Default: ...
```

# Integrations

## Console Application

```
   internal abstract class Program
   {
      internal static void Main()
      {
        var cfg = new ConfigurationLoader<Program>().Load().Build();
        //...
      }
   }
```

## AspNetCore Application

```
   internal abstract class Program
   {
      internal static void Main() => Create<Startup>().Build().Run();

      internal static IWebHostBuilder Create<T>() where T : class
      {
         return WebHost
            .CreateDefaultBuilder()
            .LoadConfiguration<T>()
            .UseStartup<T>();
      }
   }

   internal class Startup
   {
      public Startup(IConfiguration cfg) => this.cfg = cfg;

      private readonly IConfiguration cfg;

      public void ConfigureServices(IServiceCollection services)
      {
         services
           .AddMvc()
           .SetCompatibilityVersion(CompatibilityVersion.Latest);
      }

      public void Configure(IApplicationBuilder application)
      {
         application
           .UsePathBaseRoot(cfg)
           .UseMvc();
      }
   }
```

## Logging

`Configuration:Debug=true`

# Examples

```
/work # dotnet run -p TestAppConsole --environment=Development --includes=Part1,Part2 --profiles=Feature1,Feature2 --EnvironmentPrefix=ASPNETCORE_,APP_
Loading configuration: (disable this using Configuration:Debug=false)
- Environment: Development
- Includes: [Part1, Part2]
- Profiles: [Feature1, Feature2]
- BasePath: /work/TestAppConsole/bin/Debug/netcoreapp2.2
- ApplicationDirectory: /root/.JRevolt.Configuration.TestAppConsole
- WorkingDirectory: /work
Sources:
- [embedded] appsettings.yaml
- [embedded] appsettings.Development.yaml (unavailable)
- [embedded] appsettings.Part1.yaml (unavailable)
- [embedded] appsettings.Part2.yaml (unavailable)
- [embedded] appsettings.Feature1.yaml (unavailable)
- [embedded] appsettings.Feature2.yaml (unavailable)
- [embedded] appsettings.local.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.Development.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.Part1.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.Part2.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.Feature1.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.Feature2.yaml (unavailable)
- [file] /work/TestAppConsole/bin/Debug/netcoreapp2.2/appsettings.local.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.Development.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.Part1.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.Part2.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.Feature1.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.Feature2.yaml (unavailable)
- [file] /root/.JRevolt.Configuration.TestAppConsole/appsettings.local.yaml (unavailable)
- [file] /work/appsettings.yaml (unavailable)
- [file] /work/appsettings.Development.yaml (unavailable)
- [file] /work/appsettings.Part1.yaml (unavailable)
- [file] /work/appsettings.Part2.yaml (unavailable)
- [file] /work/appsettings.Feature1.yaml (unavailable)
- [file] /work/appsettings.Feature2.yaml (unavailable)
- [file] /work/appsettings.local.yaml (unavailable)
- [env][ASPNETCORE_] URLS=http://+:80
- [env][APP_]
- [cli] environment=Development, includes=Part1,Part2, profiles=Feature1,Feature2, EnvironmentPrefix=ASPNETCORE_,APP_
```
