﻿using System;
using Serilog;
using Serilog.Events;

namespace JRevolt.Configuration.TestAppConsole;

public class Program
{
   public static void Main()
   {
      Log.Logger = new LoggerConfiguration()
         .WriteTo.Console()
         .MinimumLevel.Override("JRevolt.Configuration", LogEventLevel.Verbose)
         .CreateLogger();
      var cfg = ConfigurationLoader.Create<Program>()
         .Load()
         .Build();
      var log = Log.ForContext<Program>();
      log.Information("foo={0}, password={1}", cfg["foo"], cfg["password"]);
   }
}
