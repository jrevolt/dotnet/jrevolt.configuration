using JRevolt.Configuration.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using static JRevolt.Configuration.AspNetCore.CfgExtensions;

namespace JRevolt.Configuration.TestAppWebApi;

public class Program
{
   public static void Main()
   {
      // Environment.SetEnvironmentVariable(IISOption.PORT.EnvironmentVariableName(), "6666");
      // Environment.SetEnvironmentVariable(IISOption.APPL_PATH.EnvironmentVariableName(), "/iis");
      // Environment.SetEnvironmentVariable(IISOption.TOKEN.EnvironmentVariableName(), "xxx");

      Log.Logger = new LoggerConfiguration()
         .WriteTo.Console()
         .MinimumLevel.Override("JRevolt.Configuration", LogEventLevel.Verbose)
         .CreateBootstrapLogger();

      var cfg = CreateDefaultConfigurationLoader<Program>().Load().Build();
      var builder = CreateDefaultWebApplicationBuilder(cfg);
      builder.WebHost.UseSerilog((_, logcfg) => logcfg.ReadFrom.Configuration(cfg));
      builder.WebHost.UseSetting("foo", "bar");
      builder.Services.AddControllers();

      var app = builder.Build();
      app.UseConfiguration();
      app.UseRouting();
      app.MapControllers();
      app.Run();

// GET localhost:5000/api/values

   }
}

