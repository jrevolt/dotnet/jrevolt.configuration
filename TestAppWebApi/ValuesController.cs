﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Configuration.TestAppWebApi
{
   [ExcludeFromCodeCoverage]
   [Route("api/[controller]")]
   [ApiController]
   public class ValuesController : ControllerBase
   {
      [HttpGet]
      public ActionResult<IEnumerable<string>> Get(IConfiguration cfg)
      {
         return new[] {"value1", "value2", cfg["foo"]};
      }
   }
}
