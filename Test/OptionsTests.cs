using System;
using System.Collections.Generic;
using FluentAssertions;
using JRevolt.Configuration.AspNetCore;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace JRevolt.Configuration.Test
{
   public class OptionsTests
   {
      [Theory]
      [TestCase(CfgOption.Debug, "Configuration:Debug")]
      [TestCase(CfgOption.Quiet, "Configuration:Quiet")]
      [TestCase(CfgOption.Includes, "Configuration:Includes")]
      [TestCase(CfgOption.Profiles, "Configuration:Profiles")]
      [TestCase(CfgOption.ApplicationDirectory, "Configuration:ApplicationDirectory")]
      [TestCase(CfgOption.EnvironmentPrefix, "Configuration:EnvironmentPrefix")]
      public void CfgName_PropertyName(CfgOption name, string result)
      {
         name.PropertyName().Should().Be(result);
      }

      [Theory]
      [TestCase(Option.Environment, "Environment")]
      [TestCase(Option.ApplicationName, "ApplicationName")]
      [TestCase(Option.Urls, "Urls")]
      [TestCase(Option.Configuration, "Configuration")]
      public void Option_PropertyName(Option name, string result)
      {
         name.PropertyName().Should().Be(result);
      }

      [Theory]
      [TestCase(IISOption.PORT, "PORT")]
      [TestCase(IISOption.APPL_PATH, "APPL_PATH")]
      [TestCase(IISOption.TOKEN, "TOKEN")]
      public void IISOption_PropertyName(IISOption name, string result)
      {
         name.PropertyName().Should().Be(result);
      }

      [Theory]
      [TestCase(IISOption.PORT, "ASPNETCORE_PORT")]
      [TestCase(IISOption.APPL_PATH, "ASPNETCORE_APPL_PATH")]
      [TestCase(IISOption.TOKEN, "ASPNETCORE_TOKEN")]
      public void IISOption_EnvironmentVariableName(IISOption name, string result)
      {
         name.EnvironmentVariableName().Should().Be(result);
      }

      [Theory]
      [TestCase(IISOption.PORT, "PORT")]
      [TestCase(IISOption.APPL_PATH, "APPL_PATH")]
      [TestCase(IISOption.TOKEN, "TOKEN")]
      public void IISOption_GetConfigurationProperty(IISOption option, string key)
      {
         var cfg = new ConfigurationBuilder().AddInMemoryCollection().Build();
         var value = DateTime.Now.ToLongDateString();
         cfg[key] = value;
         cfg.GetConfigurationProperty(option).Should().Be(value);
      }

      [Theory]
      public void WithResolver()
      {
         var cfg = new ConfigurationBuilder()
            .AddInMemoryCollection(new Dictionary<string, string>
            {
               {CfgOption.ResolveReferences.PropertyName(), "true"},
               {"a","1"},
               {"b","2"},
               {"c","${a}+${b}"},
            }!)
            .AddResolver()
            .Build();
         cfg["c"].Should().BeEquivalentTo("1+2");
      }

   }
}
