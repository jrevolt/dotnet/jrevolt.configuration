using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.CommandLine;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using FluentAssertions;
using JRevolt.Configuration.CommandLine;
using NUnit.Framework;

namespace JRevolt.Configuration.Test
{
   public class CommandLineTests
   {
      private CliBuilder cliBuilder => new(typeof(CommandLineTests)) { TypeFilter = DuplicateRootIgnoringTypeFilter };

      private Command Cmd => cliBuilder.Build();

      private static bool DuplicateRootIgnoringTypeFilter(Type t) => t != typeof(DuplicateRoot);

      [Theory]
      public void SetReadOnlyProperty() => Cmd.Invoke("test --read-only-property=2").Should().Be(2);

      [Theory]
      public void SetWritableProperty() => Cmd.Invoke("test --writable-property=3").Should().Be(3);

      [Theory]
      public void SetShortOption() => Cmd.Invoke("test -s 4").Should().Be(4);

      [Theory]
      public void SetLongOption() => Cmd.Invoke("test --long 5").Should().Be(5);

      [Theory]
      public void DualOption()
      {
         Cmd.Invoke("test -d 6").Should().Be(6);
         Cmd.Invoke("test --dual 7").Should().Be(7);
      }

      [Theory]
      public void NamedCommand() => Cmd.Invoke("named").Should().Be(0);

      [Theory]
      public void RequiredOption() => Cmd.Invoke("required-option --required=0").Should().Be(0);

      [Theory]
      public void ValidatedOption() => Cmd.Invoke("validated-option --value=abcdef").Should().Be(0);

      [Theory]
      public void ValidatedOption_InvalidInput() => Cmd.Invoke("validated-option --value=INVALID").Should().Be(1);

      [Theory]
      public void BooleanOption()
      {
         Cmd.Invoke("boolean-option --debug").Should().Be(0);
         Cmd.Invoke("boolean-option --debug=true").Should().Be(0);
         Cmd.Invoke("boolean-option --debug=false").Should().Be(1);
      }

      [Theory]
      public void SubCommand() => Cmd.Invoke("parent sub").Should().Be(0);

      [Theory]
      public void SubSubCommand() => Cmd.Invoke("parent sub sub-sub").Should().Be(0);

      [Theory]
      public void RemainderInjection() => Cmd.Invoke("remainder -- hey=HEY lol=LOL").Should().Be(0);

      [Theory]
      public void RootCommand() => Cmd.Invoke("--dry-run").Should().Be(0);

      [Theory]
      public void CliConverterTest() => Cmd.Invoke("cli-converter -p:hey=HEY -p:lol=LOL").Should().Be(0);

      [Theory]
      public void InheritedOptionsTest() => Cmd.Invoke("cli-command-with-inherited-options -o=xxx").Should().Be(0);

      public static IEnumerable<object[]> CliOptionAttributeData()
      {
         yield return new object[] { "-s,--long", "-s", "--long" };
         yield return new object[] { "-s", "-s", null! };
         yield return new object[] { "--long", null!, "--long" };
         yield return new object[] { "-s,--", "-s", null! };
      }

      [Theory, TestCaseSource(nameof(CliOptionAttributeData))]
      public void CliOptionAttribute(string? forms, string? s, string? l)
      {
         var opt = new CliOptionAttribute(forms);
         if (s == null) opt.Short.Should().BeEmpty();
         else  opt.Short.Should().Contain(s);
         if (l == null) opt.Long.Should().BeEmpty();
         else opt.Long.Should().Contain(l);
      }

      [Theory]
      public void CliCommand_Description() => new CliCommandAttribute{Description = "hey"}.Description.Should().Be("hey");

      [Theory]
      public void CliOption_Description() => new CliOptionAttribute {Description = "hey"}.Description.Should().Be("hey");

      [Theory]
      public void CliBuilder_Factory()
      {
         var items = new List<Type>();

         object? Factory(Type t)
         {
            items.Add(t);
            return Activator.CreateInstance(t);
         }

         cliBuilder.WithCliCommandFactory(Factory).Build().Invoke("--dry-run");
         items.Should().NotBeEmpty();
      }

      [Theory] public void CliBuilder_Assemblies()
      {
         new CliBuilder(Assembly.GetExecutingAssembly()).Build().Children.Should().NotBeEmpty();
      }

      [Theory]
      public void CliBuilder_DuplicateRoots()
      {
         CliBuilder.Create<CommandLineTests>().Build().Children
            .Select(x => x.Name)
            .Should().BeEquivalentTo("root,duplicate-root,cli-command-with-inherited-options".Split(","));
      }

      [Theory]
      public void CliException_Coverage()
      {
         Assert.Throws<CliException>(() => throw new CliException("hey!"))!.Message.Should().Be("hey!");
      }

   }

   //

   [CliCommand(Root=true)]
   internal class Root
   {
      [CliOption]
      public bool DryRun { get; }

      [CliEntrypoint]
      internal virtual int Run() => DryRun ? 0 : 1;
   }

   [CliCommand(Root = true)]
   internal class DuplicateRoot
   {
   }


   [CliCommand]
   internal class TestCommand : Root
   {
      [CliOption]
      public int ReadOnlyProperty { get; }

      [CliOption]
      public int WritableProperty { get; set; }

      [CliOption("-s")]
      public int ShortOption { get; }

      [CliOption("--long")]
      public int LongOption { get; }

      [CliOption("-d,--dual")]
      public int DualOption { get; }

      [CliEntrypoint]
      internal override int Run()
      {
         return
            ReadOnlyProperty > 0 ? ReadOnlyProperty :
            WritableProperty > 0 ? WritableProperty :
            ShortOption > 0 ? ShortOption :
            LongOption > 0 ? LongOption :
            DualOption > 0 ? DualOption :
            0;
      }
   }

   [CliCommand("named")]
   internal class NamedCommand : Root
   {
      internal override int Run() => 0;
   }

   [CliCommand]
   internal class RequiredOption : Root
   {
      [CliOption, Required]
      public int Required { get; }

      internal override int Run() => 0;
   }

   [CliCommand]
   internal class BooleanOption : Root
   {
      [CliOption]
      public bool Debug { get; }

      [CliEntrypoint]
      internal override int Run() => Debug ? 0 : 1;
   }

   [CliCommand]
   internal class ValidatedOption : Root
   {
      [CliOption, Required, RegularExpression("[a-z]{3,6}")]
      public string? Value { get; }

      internal override int Run() => 0;
   }

   [CliCommand]
   internal class ParentCommand : Root
   {
      [CliEntrypoint] internal new int Run() => 0;
   }

   [CliCommand]
   internal class SubCommand : ParentCommand
   {
      [CliEntrypoint] internal new int Run() => 0;
   }

   [CliCommand]
   internal class SubSubCommand : SubCommand
   {
      [CliEntrypoint] internal new int Run() => 0;
   }

   [CliCommand]
   internal class RemainderCommand : Root
   {
      [CliRemainder] public string[] Remainder { get; } = null!;

      [CliEntrypoint]
      internal override int Run() => Remainder.SequenceEqual(new[] {"hey=HEY", "lol=LOL"}) ? 0 : 1;
   }


   [CliCommand]
   internal class CliConverterCommand : Root
   {
      [CliOption("-p,--")]
      public IDictionary<string,string> Properties { get; } = null!;

      [CliEntrypoint]
      internal override int Run()
      {
         return Properties
            .Select(x => $"{x.Key}={x.Value}")
            .ToImmutableSortedSet()
            .Aggregate((x, next) => $"{x},{next}")
            .Equals("hey=HEY,lol=LOL")
            ? 0 : 1;
      }
   }

   internal abstract class CliCommandWithInheritedOptionsCommandParent
   {
      [CliOption("-o")] public string? Opt { get; }
   }

   [CliCommand]
   internal class CliCommandWithInheritedOptionsCommand : CliCommandWithInheritedOptionsCommandParent
   {

      [CliEntrypoint] internal int Run() => Opt is not null ? 0 : 100;
   }

}
