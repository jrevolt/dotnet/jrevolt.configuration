using System;
using System.Collections.Generic;
using FluentAssertions;
using Microsoft.Extensions.Configuration;
using NUnit.Framework;

namespace JRevolt.Configuration.Test
{
   public class UtilsTests
   {
      [Theory]
      [TestCase("", "")] // empty
      [TestCase("key=a,b,c", "a,b,c")] // single CSV
      [TestCase("key:0=a;key:1=b;key:2=c", "a,b,c")] // single array
      [TestCase("key=a,b;key:0=c;key:1=d", "a,b,c,d")] // merged
      [TestCase("key=a,b,c;key:0=b;key:1=c;key:2=d", "a,b,c,d")] // merged distinct
      public void GetDualList(string src, string dst)
      {
         var cfg = new ConfigurationBuilder().AddCommandLine(src.Split(";", StringSplitOptions.RemoveEmptyEntries)).Build();
         var dstarr = dst.Split(",", StringSplitOptions.RemoveEmptyEntries);
         cfg.GetDualList("key").Should().BeEquivalentTo(dstarr);
      }


      [Theory]
      [TestCase("a,b,c", "a,b,c")] // noop
      [TestCase("a, b, c", "a,b,c")] // trimming
      [TestCase("a,a,b,b,c,c", "a,b,c")] // duplicates
      [TestCase("a,b,-c", "a,b")] // no exclude definitions
      [TestCase("a,b,c,-c", "a,b")] // no excludes
      [TestCase("a,b,c,-c,c", "a,b,c")] // re-include
      [TestCase("a,b,c,-c,c,-c", "a,b")] // re-exclude
      public void ProcessExcludes(string src, string dst)
      {
         var srcarr = src.Split(",");
         var dstarr = dst.Split(",");
         srcarr.FilterExcludes().Should().BeEquivalentTo(dstarr);
      }

      [Theory]
      public void ResolveReferences()
      {
         var cfg = new ConfigurationBuilder()
            .AddInMemoryCollection(new Dictionary<string, string>
            {
               {"foo", "a=${a}, b=${b}, c=${c}"},
               {"a", "1"},
               {"b", "2"},
               {"c", "${a}+${b}"},
               {"d", "${bar}"},
               {"e", "\\${a}"},
            }!)
            .Build();
         cfg["a"]!.ResolveReferences(cfg).Should().BeEquivalentTo("1");
         cfg["b"]!.ResolveReferences(cfg).Should().BeEquivalentTo("2");
         cfg["c"]!.ResolveReferences(cfg).Should().BeEquivalentTo("1+2");
         cfg["d"]!.ResolveReferences(cfg).Should().BeEquivalentTo("${bar}");
         cfg["e"]!.ResolveReferences(cfg).Should().BeEquivalentTo("${a}");
         cfg["foo"]!.ResolveReferences(cfg).Should().BeEquivalentTo("a=1, b=2, c=1+2");
      }

      [Theory]
      public void ConfigurationEntryKeyComparer()
      {
         new ConfigurationEntryKeyComparer()
            .Equals(new KeyValuePair<string, string>("a", "1"), new KeyValuePair<string, string>("a", "1".Clone().ToString()!))
            .Should().BeTrue();
      }

   }
}
