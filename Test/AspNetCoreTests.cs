using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using JRevolt.Configuration.AspNetCore;
using JRevolt.Configuration.TestAppWebApi;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using NUnit.Framework;

namespace JRevolt.Configuration.Test
{
   [Route("api/[controller]")]
   [ApiController]
   public class ValuesController : ControllerBase
   {
      [HttpGet]
      public ActionResult<IEnumerable<string>> Get() => new[] { "value1", "value2" };
   }


   // ReSharper disable once ClassNeverInstantiated.Global
   public class CustomWebAppFactory : WebApplicationFactory<AspNetCoreTests>
   {
      private IConfiguration cfg;

      protected override IWebHostBuilder? CreateWebHostBuilder()
      {
         cfg = AspNetCore.CfgExtensions.CreateDefaultConfigurationLoader<AspNetCoreTests>().Load().Build();
         var builder = AspNetCore.CfgExtensions.CreateDefaultWebApplicationBuilder(cfg);
         builder.WebHost.UseSetting("TEST_CONTENTROOT_JREVOLT_CONFIGURATION_TEST", builder.WebHost.GetSetting("contentRoot"));
         return builder.WebHost;
      }

      // protected override void ConfigureWebHost(IWebHostBuilder builder)
      // {
      //    builder
      //       //.UseContentRoot(Environment.CurrentDirectory)
      //       // .UseConfiguredEndpoint(cfg)
      //       // //.UseStartup<Startup>()
      //       .ConfigureServices(services => services
      //          .AddControllers())
      //       // .Configure(app => app
      //       //    .UseConfiguration()
      //       //    .UseRouting()
      //       //    .UseEndpoints(ep => ep.MapControllers()))
      //       ;
      //    return;
      // }
   }


   [TestFixture]
   public class AspNetCoreTests //: IClassFixture<CustomWebAppFactory>
   {
      //private WebApplicationFactory<AspNetCoreTests> factory;

      private Task<IHost> CreateTestHost(Action<IConfiguration> configure)
      {
         var cfg = AspNetCore.CfgExtensions.CreateDefaultConfigurationLoader<Program>().Load().Build();
         configure(cfg);
         return new HostBuilder()
            .ConfigureHostConfiguration(x => x.AddConfiguration(cfg))
            .ConfigureWebHost(builder => builder
               .UseTestServer()
               .UseConfiguredEndpoint(cfg)
               .ConfigureServices(services => services.AddControllers())
               .Configure(app => app
                  .UseConfiguration()
                  .UseRouting()
                  .UseEndpoints(ep => ep.MapControllers())))
            .StartAsync();
      }

      [Theory]
      [TestCase("")]
      [TestCase("/root")]
      public async Task ControllerTest(string root)
      {
         using var host = await CreateTestHost(cfg => cfg["urls"] = $"http://+:80{root}");
         var client = host.GetTestClient();
         var result = await client.GetAsync($"{root}/api/values");
         result.IsSuccessStatusCode.Should().BeTrue();
      }

      [Theory]
      public async Task UsePathBaseRoot_NotFound()
      {
         using var host = await CreateTestHost(cfg => cfg["urls"] = "http://+:80/root");
         using var client = host.GetTestClient();
         var result = await client.GetAsync("/root/api/values");
         result.StatusCode.Should().Be(HttpStatusCode.OK);
         result = await client.GetAsync("/notfound/api/values");
         result.StatusCode.Should().Be(HttpStatusCode.NotFound);
         result = await client.GetAsync("/api/values");
         result.StatusCode.Should().Be(HttpStatusCode.NotFound);
      }

      [Theory]
      public void UseSettingTest()
      {
         const string key = nameof(UseSettingTest);
         const string value = "TestValue";
         var cfg = AspNetCore.CfgExtensions.CreateDefaultConfigurationLoader<AspNetCoreTests>().Load().Build();
         var host = Host.CreateDefaultBuilder()
            .ConfigureHostConfiguration(x => x.AddConfiguration(cfg))
            .ConfigureWebHost(x => x.UseSetting(key, value))
            .Build();
         var cfg2 = host.Services.GetService<IConfiguration>()!;
         cfg2[key].Should().Be(value);
      }

      [Theory]
      [TestCase("http://+:5000", "http://+:5000", "")]
      [TestCase("http://+:5000/root", "http://+:5000", "/root")]
      [TestCase("http://+:5000/root/subpath", "http://+:5000", "/root/subpath")]
      public void CfgExtensions_GetUrls_GetPathBase(string configured, string urls, string pathbase)
      {
         var cfg = new ConfigurationBuilder().AddInMemoryCollection().Build();
         cfg["urls"] = configured;
         AspNetCore.CfgExtensions.GetUrls(cfg).Should().Be(urls);
         AspNetCore.CfgExtensions.GetPathBase(cfg).Should().Be(pathbase);
      }

      // [Theory]
      // public void CfgExtensions_FixUrls()
      // {
      //    using var c = new TestWebAppFactory()
      //       //.WithWebHostBuilder(builder => builder.Configure(app => { app.FixUrls(); }))
      //       .CreateClient();
      // }
   }

   internal class TestStartup
   {
   }

   internal class TestWebAppFactory : WebApplicationFactory<TestStartup>
   {
      protected override IWebHostBuilder CreateWebHostBuilder()
      {
         return WebHost.CreateDefaultBuilder()
            .LoadConfiguration<TestStartup>()
            .UseStartup<TestStartup>();
      }
   }
}
