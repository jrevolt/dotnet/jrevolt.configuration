using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using FluentAssertions;
using JRevolt.Configuration.TestAppConsole;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.FileProviders;
using NetEscapades.Configuration.Yaml;
using NUnit.Framework;

namespace JRevolt.Configuration.Test
{
   public class MainTests
   {
      // ReSharper disable once MemberCanBePrivate.Global
      [ExcludeFromCodeCoverage]
      public static IEnumerable<object[]> Repeat()
      {
         for (var i = 1; i <= 5; i++) yield return new object[] {i};
      }

      [Theory]
      [TestCaseSource(nameof(Repeat))]
      public void FullTest(int i)
      {
         //i.Should().BePositive(); // silence unused parameter warning
         ConfigurationLoader.Create<TestAppConsole.Program>().Load().Build().Should().NotBeNull();
      }

      [Theory]
      public void MultiSource_BuildFails()
      {
         Assert.Throws<InvalidOperationException>(() =>
            new MyMultiSource(null!)
               .Build(new ConfigurationBuilder()));
      }

      [Theory]
      public void ReplaceX()
      {
         "input".ReplaceX("^.*$", "replacement").Should().Be("replacement");
      }

      [Theory]
      public void OptionalFileProviderTest()
      {
         new OptionalFileProvider(".").GetDirectoryContents("/").Exists.Should().BeTrue();
      }

      [Theory]
      public void MyFileProviderTest()
      {
         var t = typeof(Program);
         new MyFileProvider(new EmbeddedFileProvider(t.Assembly, t.Namespace)).GetDirectoryContents("/").Exists
            .Should().BeTrue();
      }

      [Theory]
      public void MyCfgProviderTest()
      {
         var data = new MemoryConfigurationProvider(new MemoryConfigurationSource());
         var provider = new MyCfgProvider(() => data);
         provider.Set("key", "value");
         provider.TryGet("key", out var value);
         value.Should().Be("value");
      }


      // ReSharper disable once MemberCanBePrivate.Global
      [ExcludeFromCodeCoverage]
      public static IEnumerable<object[]> GetValueListData()
      {
         yield return new object[] {"key=v1,v2", "v1;v2"};
         yield return new object[] {"key:0=0;key:1=1", "0;1"};

         // combined array+csv?
         yield return new object[] {"key=v1,v2;key:0=0;key:1=1", "0;1;v1;v2"};
         yield return new object[] {"key:0=0;key:1=1;key=v1,v2", "0;1;v1;v2"};

         // duplicates?
         yield return new object[] {"key=1,1,2,2", "1;2"};

         // whitespace?
         yield return new object[] {"key:0= 0 ;key:1= 1 ", "0;1"};

         // ordering?
         yield return new object[] {"key:0=0;key:1=1;key:2=2", "0;1;2"};
         yield return new object[] {"key:2=2;key:1=1;key:0=0", "0;1;2"};
      }

      [Theory]
      [TestCaseSource(nameof(GetValueListData))]
      public void ConfigurationLoader_GetValueList(string input, string expected)
      {
         var cfg = new ConfigurationBuilder()
            .AddCommandLine(input.Split(";"))
            .Build();
         cfg.GetDualList("key")
            .Should().BeEquivalentTo(expected.Split(";"));
      }

      [Theory]
      public void ConfigurationLoader_AddSources()
      {
         var provider = new MyFileProvider(Directory.GetCurrentDirectory);
         var loader = ConfigurationLoader.Create<Program>();
         loader.Init();
         loader.AddSource(provider, "name", () => new[] {"item1", "item2"});
      }

      [Theory]
      public void ConfigurationLoader_OnLoadException()
      {
         Assert.Throws<ConfigurationException>(() =>
            ConfigurationLoader.OnLoadException(
               new FileLoadExceptionContext
               {
                  Provider = new YamlConfigurationProvider(new YamlConfigurationSource
                  {
                     FileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory()),
                     Path = "fake.yaml",
                     Optional = false,
                  }),
                  Exception = new ApplicationException(),
               }));
      }

      [ExcludeFromCodeCoverage]
      public static IEnumerable<object[]> DescribeData()
      {
         yield return new object[]
         {
            new DefaultsConfigurationProvider(new Dictionary<string, string>()),
            "defaults",
         };
         yield return new object[]
         {
            new YamlConfigurationProvider(new YamlConfigurationSource()),
            "embedded|file"
         };
         yield return new object[]
         {
            new MyEnvConfigurationProvider("X_"),
            "env"
         };
         yield return new object[]
         {
            new MyCliConfigurationProvider(new DefaultCliConfigurationProvider()),
            "cli"
         };
         yield return new object[]
         {
            new MyCliConfigurationProvider(new CustomCliConfigurationProvider(new Dictionary<string, string>())),
            "cli"
         };
         yield return new object[]
         {
            new ResolverConfigurationProvider(new Dictionary<string, string>()),
            "resolver"
         };
         yield return new object[]
         {
            new DecryptionConfigurationProvider(new Dictionary<string, string>()),
            "secrets"
         };
         yield return new object[]
         {
            new WritableConfigurationProvider(),
            "memory"
         };
      }

      [Theory]
      [TestCaseSource(nameof(DescribeData))]
      public void Describe(IConfigurationProvider provider, string match)
      {
         new MySource(() => provider).Describe().Should().MatchRegex(match);
      }

      [Theory]
      public void Describe_UnexpectedProvider()
      {
         Assert.Throws<ConfigurationException>(() =>
            new MySource(() => new ChainedConfigurationProvider(
               new ChainedConfigurationSource
               {
                  Configuration = new ConfigurationRoot(ImmutableList<IConfigurationProvider>.Empty),
               }))
               .Describe());
      }

      [Theory]
      [TestCase(false, true)]
      [TestCase(false, false)]
      [TestCase(true, false)]
      public void LogConfigInfo(bool debug, bool quiet)
      {
         var loader = ConfigurationLoader.Create<MainTests>();
         var cfg = new ConfigurationBuilder().AddInMemoryCollection().Build();
         cfg["Configuration:Debug"] = debug.ToString();
         cfg["Configuration:Quiet"] = quiet.ToString();
         var src = new List<IConfigurationSource>
         {
            new MySource(() => new WritableConfigurationProvider())
         };
         loader.LogConfigInfo(cfg, src);
      }

      [Theory]
      public void MySource_GetData()
      {
         var map = new Dictionary<string, string>
         {
            {"key1", "value1"},
            {"key2", "value2"},
         };
         MySource.GetData(map).Should().MatchRegex("key1=value1.*key2=value2");
      }

      [Theory]
      public void ConfigurationLoader_UseCommandLine()
      {
         ConfigurationLoader.Create<MainTests>()
            .UseCommandLine(new Dictionary<string, string> {{"aaa", "bbb"}})
            .Load()
            .Build()["aaa"].Should().Be("bbb");
      }

      [Theory]
      public void ConfigurationLoader_References()
      {
         ConfigurationLoader.Create<MainTests>()
            .UseCommandLine(new Dictionary<string, string>
            {
               {CfgOption.ResolveReferences.PropertyName(), "true"},
               {"a", "1"},
               {"b", "2"},
               {"c", "${a}+${b}"}
            })
            .Load()
            .Build()["c"].Should().Be("1+2");
      }
   }
}
