using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.CommandLine;

namespace JRevolt.Configuration
{
   public interface IDataProvider
   {
      IDictionary<string, string> GetData();
   }



   /// <summary>
   /// Default CLI provider which parses actual process command line
   /// </summary>
   public class DefaultCliConfigurationProvider : CommandLineConfigurationProvider, IDataProvider
   {
      public DefaultCliConfigurationProvider() : base(Environment.GetCommandLineArgs().Skip(1).ToArray())
      {
      }

      public IDictionary<string, string> GetData() => Data;
   }

   /// <summary>
   /// Custom CLI provider which assumes command line is processed by other means, and configuration properties are extracted, parsed,
   /// and provided as key/value pairs
   /// </summary>
   /// <see cref="ConfigurationLoader{T}.CliConfigurationProvider"/>
   public class CustomCliConfigurationProvider : ConfigurationProvider, IDataProvider
   {
      public CustomCliConfigurationProvider(IDictionary<string, string> data) => Data = data;

      public IDictionary<string, string> GetData() => Data;
   }
}
