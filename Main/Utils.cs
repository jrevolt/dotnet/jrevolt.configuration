using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JRevolt.YamlSecrets;
using Microsoft.Extensions.Configuration;

namespace JRevolt.Configuration
{
   internal static class Utils
   {
      internal static IEnumerable<string> GetDualList(this IConfiguration cfg, CfgOption option)
         => cfg.GetDualList(option.PropertyName());

      internal static IEnumerable<string> GetDualList(this IConfiguration cfg, string key)
      {
         var result = new List<string>();
         var list = cfg.GetSection(key);

         // first entry represents literal interpreted as CSV
         list.AsEnumerable().First().Value?
            .Split(",", StringSplitOptions.RemoveEmptyEntries)
            .ForEach(x => result.Add(x.Trim()));

         // rest of the items is reverse-sorted array
         list.AsEnumerable().Skip(1).Reverse().ForEach(x => result.Add(x.Value.Trim()));

         return result.Distinct().ToList();
      }

      internal static IEnumerable<string> FilterExcludes(this IEnumerable<string> items)
      {
         var list = items
            .Select(x => x.Trim())
            .ToList();
         return list
               // remove excluded
               .Where(x => list.LastIndexOf(x) > list.LastIndexOf($"-{x}"))
               // remove exclude requests
               .Where(x => !x.StartsWith("-"))
               // remove duplicates
               .Distinct()
               .ToList()
            ;
      }

      internal static IEnumerable<string> BuildIncrementalList(this IList<IMySource> sources, CfgOption key)
      {

         var list = new List<string>();
         sources.ForEach(x =>
         {
            var builder = new ConfigurationBuilder();
            if (x is MyMultiSource msrc)
            {
               msrc.Sources().ForEach(y => list.AddRange(builder.Add(y).Build().GetDualList(key)));
            }
            else
            {
               list.AddRange(builder.Add(x).Build().GetDualList(key));
            }
         });
         return list.FilterExcludes();
      }

      internal static string ResolveReferences(this string input, IConfiguration cfg)
      {
         var start = input.IndexOf("${", StringComparison.InvariantCulture);
         var ignored = start != -1 && start > 0 && input[start - 1] == '\\';
         var end = start != -1 ? input.IndexOf("}", start, StringComparison.InvariantCulture) : -1;
         if (start == -1 || end == -1) return input;

         if (ignored)
         {

            return new StringBuilder()
               .Append(input.Substring(0, start - 1)) //up to escape char
               .Append(input[start]) // add escaped char
               .Append(input.Substring(start + 1).ResolveReferences(cfg)) // resolve rest
               .ToString();
         }

         var key = input.Substring(start + 2, end-start-2);
         var value = cfg.GetSection(key).Exists() ? cfg[key].ResolveReferences(cfg) : input.Substring(start, end-start+1);
         var remainder = input.Length > end ? ResolveReferences(input.Substring(end + 1), cfg) : "";
         var result = input.Substring(0, start) + value + remainder;
         return result;
      }

      internal static KeyValuePair<string, string> DecryptValue(this KeyValuePair<string, string> entry, Secrets secrets)
      {
         try
         {
            var result = secrets.Decrypt(entry.Value);
            return KeyValuePair.Create(entry.Key, result);
         }
         catch (Exception e)
         {
            throw new ConfigurationException(e,$"Failed to decrypt key {entry.Key}, value: {entry.Value}");
         }
      }


   }

   internal class ConfigurationEntryKeyComparer : IEqualityComparer<KeyValuePair<string, string>>
   {
      public bool Equals(KeyValuePair<string, string> x, KeyValuePair<string, string> y)
      {
         return x.Key.ToLower().Equals(y.Key.ToLower());
      }

      public int GetHashCode(KeyValuePair<string, string> obj)
      {
         return obj.Key.ToLower().GetHashCode();
      }
   }

}
