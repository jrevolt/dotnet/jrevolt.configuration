using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using NetEscapades.Configuration.Yaml;
using YamlDotNet.Core;
using YamlDotNet.RepresentationModel;

namespace JRevolt.Configuration
{
   internal class YamlCfgProvider : YamlConfigurationProvider
   {
      public YamlCfgProvider(YamlConfigurationSource source) : base(source)
      {
      }

      private bool loaded;

      public override void Load(Stream stream)
      {
         if (loaded) return;

         try
         {
            var parser = new YamlCfgParser();
            Data = parser.Parse(stream);
            loaded = true;
         }
         catch (YamlException e)
         {
            throw new FormatException(e.Message, e);
         }
      }
   }

   internal class YamlCfgParser
   {
      private readonly IDictionary<string, string> data = new SortedDictionary<string, string>(StringComparer.OrdinalIgnoreCase);
      private readonly Stack<string> context = new();
      private string currentPath;

      // encrypted yaml entries are tagged !<encrypted>
      private static readonly string EncryptedTag = $"encrypted";

      // yaml tags are lost during parsing and loading into string:string kay/value pairs
      // this prefix is used to preserve the !<encrypted tag in simple string values
      public static readonly string EncryptedPrefixInline = $"\u001B!<{EncryptedTag}>\u001B";

      public IDictionary<string, string> Parse(Stream input)
      {
         var yaml = new YamlStream();
         yaml.Load(new StreamReader(input, true));

         yaml.Documents.ForEach(doc =>
         {
            if (doc.RootNode is YamlMappingNode mapping) VisitYamlMappingNode(mapping);
         });

         return data;
      }

      private void VisitYamlNodePair(KeyValuePair<YamlNode, YamlNode> yamlNodePair)
      {
         var s = ((YamlScalarNode) yamlNodePair.Key).Value!;
         VisitYamlNode(s, yamlNodePair.Value);
      }

      private void VisitYamlNode(string s, YamlNode node)
      {
         switch (node)
         {
            case YamlScalarNode scalarNode:
               VisitYamlScalarNode(s, scalarNode);
               break;
            case YamlMappingNode mappingNode:
               VisitYamlMappingNode(s, mappingNode);
               break;
            case YamlSequenceNode sequenceNode:
               VisitYamlSequenceNode(s, sequenceNode);
               break;
         }
      }

      private void VisitYamlScalarNode(string s, YamlScalarNode yamlValue)
      {
         //a node with a single 1-1 mapping
         EnterContext(s);

         var currentKey = currentPath;

         var value = IsNullValue(yamlValue) ? null : yamlValue.Value;

         var encrypted = value is not null && yamlValue.Tag.Equals(EncryptedTag);
         if (encrypted)
         {
            // no decryption here, that's done later on configuration provider
            // here we have to make sure we don't lose !<encrypted> flag, and the only way to persist this information
            // is the value itself.
            // Configuration provider will have to detect this special marker, and discard it before decrypting the actual data
            value = $"{EncryptedPrefixInline}{value}";
         }

         data[currentKey] = value!;

         ExitContext();
      }

      private void VisitYamlMappingNode(YamlMappingNode node)
      {
         node.Children.ForEach(VisitYamlNodePair);
      }

      private void VisitYamlMappingNode(string s, YamlMappingNode yamlValue)
      {
         //a node with an associated sub-document
         EnterContext(s);
         VisitYamlMappingNode(yamlValue);
         ExitContext();
      }

      private void VisitYamlSequenceNode(string s, YamlSequenceNode yamlValue)
      {
         //a node with an associated list
         EnterContext(s);
         VisitYamlSequenceNode(yamlValue);
         ExitContext();
      }

      private void VisitYamlSequenceNode(YamlSequenceNode node)
      {
         var idx = 0;
         node.Children.ForEach(x => VisitYamlNode(idx++.ToString(), x));
      }

      private void EnterContext(string s)
      {
         context.Push(s);
         currentPath = ConfigurationPath.Combine(context.Reverse());
      }

      private void ExitContext()
      {
         context.Pop();
         currentPath = ConfigurationPath.Combine(context.Reverse());
      }

      private static bool IsNullValue(YamlScalarNode yamlValue)
      {
         return yamlValue.Style == ScalarStyle.Plain && yamlValue.Value is "~" or "null" or "Null" or "NULL";
      }
   }
}
