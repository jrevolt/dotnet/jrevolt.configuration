using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Primitives;
using NetEscapades.Configuration.Yaml;

namespace JRevolt.Configuration
{
   internal interface IMySource : IConfigurationSource
   {
   }

   public class MySource : IMySource
   {
      public MySource(Func<IConfigurationProvider> provider) => this.provider = new MyCfgProvider(provider);

      internal readonly MyCfgProvider provider;

      public IConfigurationProvider Build(IConfigurationBuilder builder) => provider;

      public bool IsBuiltInSource()
      {
         return
            provider.Provider is MemoryConfigurationProvider ||
            ((provider.Provider as YamlConfigurationProvider)?.Source.FileProvider as MyFileProvider)?.IsEmbedded == true;
      }

      public bool IsAvailable()
      {
         switch (provider.Provider)
         {
            case ResolverConfigurationProvider:
            case MemoryConfigurationProvider:
            case MyEnvConfigurationProvider:
            case MyCliConfigurationProvider:
               return true;
            case YamlConfigurationProvider yaml:
               var fp = yaml.Source.FileProvider as MyFileProvider;
               var path = yaml.Source.Path;
               var fi = fp?.GetFileInfo(path);
               return Convert.ToBoolean(fi?.Exists);
            default:
               return false;
         }
      }

      internal void Detach()
      {
         provider.Detach();
      }

      internal string Describe()
      {
         // available/unavailable
         const string y = "*", n = " ";

         switch (provider.Provider)
         {
            case ResolverConfigurationProvider r:
               return $"{y}[resolver] {GetKeys(r.GetData())}";
            case DecryptionConfigurationProvider r:
               return $"{y}[secrets] {GetKeys(r.GetData())}";
            case DefaultsConfigurationProvider r:
               return $"{y}[defaults] {GetKeys(r.ToList())}";
            case YamlConfigurationProvider yaml:
               var fp = yaml.Source.FileProvider as MyFileProvider;
               var embedded = fp?.IsEmbedded == true;
               var path = yaml.Source.Path;
               var fi = fp?.GetFileInfo(path);
               var type = embedded ? "embedded" : "file";
               var label = Convert.ToBoolean(fi?.Exists) ? "" : "(unavailable)";
               var tag = Convert.ToBoolean(fi?.Exists) ? y : n;
               path = fi?.PhysicalPath ?? fi?.Name ?? path;
               return $"{tag}[{type}] {path} {label}";
            case MyEnvConfigurationProvider env:
               return $"{y}[env][{env.Prefix}] {GetKeys(env.GetData())}";
            case MyCliConfigurationProvider cli:
               return $"{y}[cli] {GetKeys(cli.GetData())}";
            case WritableConfigurationProvider r:
               return $"{y}[memory] {GetKeys(r.ToList())}";
            default:
               throw new ConfigurationException($"Unexpected provider type: {provider.Provider}");
         }
      }

      internal static string GetData(IDictionary<string, string> data)
         => string.Join(", ", data.Select(x => $"{x.Key}={x.Value}").ToList());

      internal static string GetKeys(IDictionary<string, string> data)
         => string.Join(", ", data.Select(x => x.Key));

      internal static string GetKeys(List<KeyValuePair<string, string>> data)
         => string.Join(", ", data.Select(x=>x.Key));

      [ExcludeFromCodeCoverage]
      public override string ToString() => Describe();
   }

   internal class MyMultiSource : IMySource
   {
      internal Func<IList<IConfigurationSource>> Sources { get; }

      public MyMultiSource(Func<IList<IConfigurationSource>> sources) => Sources = sources;

      public IConfigurationProvider Build(IConfigurationBuilder builder) => throw new InvalidOperationException();

      [ExcludeFromCodeCoverage]
      public override string ToString() => string.Join(",", Sources().Select(x => x.ToString()).ToList());
   }

   public class WriteIgnoringConfigurationSource : IConfigurationSource, IConfigurationProvider
   {
      private readonly IConfigurationProvider provider;

      public WriteIgnoringConfigurationSource(IConfiguration cfg)
         => provider = new ChainedConfigurationProvider(new ChainedConfigurationSource{Configuration = cfg});

      public IConfigurationProvider Build(IConfigurationBuilder builder)
         => this;

      public IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath)
         => provider.GetChildKeys(earlierKeys, parentPath);

      public IChangeToken GetReloadToken()
         => provider.GetReloadToken();

      public void Load()
         => provider.Load();

      public void Set(string key, string value)
         => Expression.Empty();

      public bool TryGet(string key, out string value)
         => provider.TryGet(key, out value);

   }


   internal class MyCfgProvider : IConfigurationProvider
   {
      public MyCfgProvider(Func<IConfigurationProvider> provider) => this.provider = provider;

      private Func<IConfigurationProvider> provider;

      internal IConfigurationProvider Provider => provider();

      internal void Detach()
      {
         var p = Provider;
         provider = () => p;
         reloadable = true;
      }

      private bool reloadable;

      public bool TryGet(string key, out string value) => Provider.TryGet(key, out value);

      public void Set(string key, string value) => Provider.Set(key, value);

      public IChangeToken GetReloadToken() => reloadable ? Provider.GetReloadToken() : NullChangeToken.Singleton;

      public void Load() => Provider.Load();

      public IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath)
         => Provider.GetChildKeys(earlierKeys, parentPath);

   }

   internal class MyFileProvider : IFileProvider
   {
      public MyFileProvider(Func<string> name) => this.name = name;

      // ReSharper disable once SuggestBaseTypeForParameter
      public MyFileProvider(EmbeddedFileProvider embedded)
      {
         name = () => "[embedded]";
         Provider = embedded;
         current = name();
      }

      private Func<string> name;

      private string? current;

      // ReSharper disable once InconsistentNaming
      private IFileProvider? _provider;

      private IFileProvider Provider
      {
         get
         {
            var uptodate = name().Equals(current);
            if (uptodate && _provider != null) return _provider;
            current = name();
            return Provider = new OptionalFileProvider(current);
         }
         set => _provider = value;
      }

      internal void Detach() => name = () => current!;

      internal string Root => current ?? name();

      internal bool IsEmbedded => Provider is EmbeddedFileProvider;

      public IFileInfo GetFileInfo(string subpath) => Provider.GetFileInfo(subpath);

      public IDirectoryContents GetDirectoryContents(string subpath) => Provider.GetDirectoryContents(subpath);

      public IChangeToken Watch(string filter) => Provider.Watch(filter);
   }

   internal class OptionalFileProvider : IFileProvider
   {
      public OptionalFileProvider(string path)
      {
         this.path = Path.GetFullPath(path);
      }

      private readonly string path;
      private IFileProvider? existing;
      private IFileProvider? dummy;

      private IFileProvider Provider
      {
         get
         {
            if (existing == null && Directory.Exists(path)) existing = new PhysicalFileProvider(path);
            return existing ?? (dummy ??= new NullFileProvider()); //NOSONAR
         }
      }

      public IFileInfo GetFileInfo(string subpath)
      {
         var fi = Provider.GetFileInfo(subpath);
         return existing != null ? fi : new NotFoundFileInfo(Path.GetFullPath(subpath, path));
      }

      public IDirectoryContents GetDirectoryContents(string subpath)
      {
         return Provider.GetDirectoryContents(subpath);
      }

      public IChangeToken Watch(string filter)
      {
         return Provider.Watch(filter);
      }
   }

   internal class MyEnvConfigurationProvider : EnvironmentVariablesConfigurationProvider, IDataProvider
   {
      internal readonly string Prefix;

      public MyEnvConfigurationProvider(string prefix) : base(prefix) => Prefix = prefix;

      public IDictionary<string, string> GetData() => Data;
   }

   internal class MyCliConfigurationProvider : IConfigurationProvider, IDataProvider
   {
      private IConfigurationProvider Provider { get; }

      public MyCliConfigurationProvider(IConfigurationProvider provider) => Provider = provider;

      public bool TryGet(string key, out string value) => Provider.TryGet(key, out value);

      [ExcludeFromCodeCoverage]
      public void Set(string key, string value) => throw new NotImplementedException("Not Supported!");

      public IChangeToken GetReloadToken() => Provider.GetReloadToken(); // todo null fails here

      public void Load() => Provider.Load();

      public IEnumerable<string> GetChildKeys(IEnumerable<string> earlierKeys, string parentPath) => Provider.GetChildKeys(earlierKeys, parentPath);

      public IDictionary<string, string> GetData() => ((IDataProvider) Provider).GetData();
   }

   internal abstract class InMemoryConfigurationProvider : MemoryConfigurationProvider, IDataProvider
   {
      protected InMemoryConfigurationProvider(IDictionary<string, string> data) : base(new MemoryConfigurationSource{InitialData = data})
      {
      }

      public IDictionary<string, string> GetData() => Data;
   }

   internal class DefaultsConfigurationProvider : InMemoryConfigurationProvider
   {
      public DefaultsConfigurationProvider(IDictionary<string, string> data) : base(data)
      {
      }
   }

   internal class ResolverConfigurationProvider : InMemoryConfigurationProvider
   {
      public ResolverConfigurationProvider(IDictionary<string, string> data) : base(data)
      {
      }
   }


   internal class DecryptionConfigurationProvider : InMemoryConfigurationProvider
   {
      public DecryptionConfigurationProvider(IDictionary<string, string> data) : base(data)
      {
      }
   }

   internal class WritableConfigurationProvider : InMemoryConfigurationProvider
   {
      public WritableConfigurationProvider() : base(new Dictionary<string, string>())
      {
      }
   }

   internal static class Extensions
   {
      internal static string ReplaceX(this string input, string pattern, string replacement)
      {
         return Regex.Replace(input, pattern, replacement); //NOSONAR
      }

      internal static IList<MyFileProvider> AddProvider(this IList<MyFileProvider> list, EmbeddedFileProvider provider)
      {
         list.Add(new MyFileProvider(provider));
         return list;
      }

      internal static IList<MyFileProvider> AddProvider(this IList<MyFileProvider> list, Func<string> item)
      {
         list.Add(new MyFileProvider(item));
         return list;
      }

      internal static void ForEach<T>(this IEnumerable<T> list, Action<T> action)
      {
         foreach (var t in list)
         {
            action(t);
         }
      }

      internal static IConfigurationBuilder AddMultiSource(this IConfigurationBuilder builder, IConfigurationSource src)
      {
         if (src is MyMultiSource msrc) msrc.Sources().ForEach(x => builder.Add(x));
         else builder.Add(src);
         return builder;
      }
   }

   [Serializable]
   public class ConfigurationException : ApplicationException
   {
      public ConfigurationException(string message, Exception? innerException = null) : base(message, innerException)
      {
      }

      public ConfigurationException(Exception cause, string message) : base(message, cause)
      {
      }
   }

}
