using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.FileProviders;
using NetEscapades.Configuration.Yaml;
using Serilog.Events;

namespace JRevolt.Configuration
{
   public class ConfigurationLoader
   {
      public static ConfigurationLoader Create<T>() => new ConfigurationLoader(typeof(T).Assembly);

      public ConfigurationLoader(Assembly assembly)
      {
         MainAssembly = assembly;
         BaseNamespace = MainAssembly.EntryPoint?.DeclaringType?.Namespace!;
         Init();
         UseDefault(Option.Environment, "NoEnvironment");
         UseDefault(Option.ApplicationName, MainAssembly.GetName().Name!);
         UseDefault(CfgOption.EnvironmentPrefix, "X_");
         UseDefault(CfgOption.ApplicationDirectory, $"{Path.GetFullPath($".{ApplicationName}", HomePath)}");
      }

      public IConfigurationProvider CliConfigurationProvider { get; set; } = new DefaultCliConfigurationProvider();

      //public bool ResolveReferences { get; set; }

      private Assembly MainAssembly { get; }
      private string BaseNamespace { get; }

      // dynamic properties//

      private string ApplicationName => cfg.GetConfigurationProperty(Option.ApplicationName) ?? MainAssembly.GetName().Name!;

      private string BasePath => Path.GetDirectoryName(MainAssembly.Location)!;

      private static string HomePath => RuntimeInformation.IsOSPlatform(OSPlatform.Windows) //NOSONAR
         ? System.Environment.GetEnvironmentVariable("USERPROFILE")!
         : System.Environment.GetEnvironmentVariable("HOME")!
         ;

      private string ApplicationDirectory =>
         cfg.GetConfigurationProperty(CfgOption.ApplicationDirectory)?.ReplaceX("^~", HomePath)
         ?? $"{Path.GetFullPath($".{ApplicationName}", HomePath)}";

      private static string WorkingDirectory => Directory.GetCurrentDirectory(); //NOSONAR

      private string? Environment { get; set; }

      private IEnumerable<string> EnvironmentPrefix => cfg.GetDualList(CfgOption.EnvironmentPrefix);

      private IEnumerable<string> Includes { get; set; } = new List<string>();

      private IEnumerable<string> Profiles { get; set; } = new List<string>();

      // working data //

      // intermediate cfg snapshots
      private IConfiguration cfg = null!;

      private IDictionary<string, IConfigurationProvider> providerIndex = null!;

      private IList<IMySource> sources = null!;

      private readonly IDictionary<string, string> defaults = new Dictionary<string, string>();

      private ConfigurationLoader Use(Action action)
      {
         action.Invoke();
         return this;
      }

      public ConfigurationLoader UseDefault(Option option, string value) => UseDefault(option.PropertyName(), value);

      public ConfigurationLoader UseDefault(CfgOption option, string value) => UseDefault(option.PropertyName(), value);

      public ConfigurationLoader UseDefault(string key, string value) => Use(() => defaults[key] = value);

      //public ConfigurationLoader EnableReferences() => Use(() => ResolveReferences = true);

      public ConfigurationLoader UseCommandLine(IDictionary<string, string> args) => Use(() =>
      {
         var copy = new Dictionary<string, string>(args, StringComparer.OrdinalIgnoreCase);
         CliConfigurationProvider = new CustomCliConfigurationProvider(copy);
      });

      public IConfigurationBuilder Load(ConfigurationBuilder? builder = null)
      {
         builder ??= new ConfigurationBuilder();

         Init();

         // defaults
         AddSource(() => ResolveConfigurationProvider(
            "[defaults]",
            () => new DefaultsConfigurationProvider(defaults)));

         var fproviders = new List<MyFileProvider>()
            .AddProvider(new EmbeddedFileProvider(MainAssembly, BaseNamespace))
            .AddProvider(() => BasePath)
            .AddProvider(() => ApplicationDirectory)
            .AddProvider(() => WorkingDirectory)
            .ToList();

         fproviders.ForEach(p =>
         {
            const string name = "appsettings";
            AddSource(p, () => $"{name}.yaml");
            AddSource(p, () => $"{name}.{Environment}.yaml");
            AddSource(p, name, () => Includes);
            AddSource(p, name, () => Profiles);
            AddSource(p, () => $"{name}.local.yaml");
         });

         AddEnvSource();
         AddCliSource();

         var tmp = Populate(new ConfigurationBuilder());

         Cleanup(fproviders, tmp);

         LogConfigInfo(tmp.Build(), tmp.Sources);

         // make it properly writable:
         // ignore all writes in current sources, and add writable layer on top
         var ro = new ConfigurationBuilder();
         tmp.Sources.ForEach(x => ro.Add(x));
         builder.Add(new WriteIgnoringConfigurationSource(ro.Build()));
         builder.Add(new MemoryConfigurationSource());

         return builder;
      }

      internal void Init()
      {
         cfg = new ConfigurationBuilder().Build();
         providerIndex = new Dictionary<string, IConfigurationProvider>();
         sources = new List<IMySource>();
      }

      private void Cleanup(List<MyFileProvider> providers, IConfigurationBuilder builder)
      {
         providers.ForEach(x => x.Detach());
         builder.Sources.ForEach(x => (x as MySource)?.Detach());
         sources = null!;
         providerIndex = null!;
         cfg = null!;
      }

      internal void LogConfigInfo(IConfiguration config, IEnumerable<IConfigurationSource> src)
      {
         try
         {
            cfg = config; // required by referenced properties

            var log = Serilog.Log.ForContext(GetType());
            log.Debug("- Environment: {0}", Environment!);
            log.Debug("- Includes: [{0}]", string.Join(", ", Includes));
            log.Debug("- Profiles: [{0}]", string.Join(", ", Profiles));
            log.Debug("- BasePath: {0}", BasePath);
            log.Debug("- ApplicationDirectory: {0}", ApplicationDirectory);
            log.Debug("- WorkingDirectory: {0}", WorkingDirectory);

            LogEventLevel SelectLevel(MySource x) => x.IsAvailable() ? LogEventLevel.Debug : LogEventLevel.Verbose;

            log.Debug("Sources:");
            src.Select(x => (MySource) x)
               .Select(x => new { level = SelectLevel(x), desc = x.Describe()})
               .ForEach(x => log.Write(x.level, "- {0}", x.desc));

            // find secrets
            var secrets = src
               .Where(x => (x as MySource)?.provider.Provider is DecryptionConfigurationProvider)
               .SelectMany(x => new ConfigurationBuilder()
                  .Add(x).Build().AsEnumerable()
                  .Select(kp => kp.Key)
                  .ToHashSet()
               )
               .ToHashSet()
               ;
            var maskSecrets = Convert.ToBoolean(cfg[CfgOption.MaskSecrets.PropertyName()] ?? "true");


            log.Verbose("Values:");

            bool IsMasked(string key) =>
               secrets.Contains(key) ||
               key.ToLower().Equals(CfgOption.PrivateKey.PropertyName().ToLower())
               ;

            cfg.AsEnumerable()
               // case is ignored by API but AsEnumerable() call returns case-sensitive duplicates; need to consolidate
               .Distinct(new ConfigurationEntryKeyComparer())
               .OrderBy(x => x.Key)
               .ToList()
               .ForEach(x =>
               {
                  var (key, value) = x;
                  if (value == null!)
                     log.Verbose("- {0}", key);
                  else if (maskSecrets && IsMasked(key))
                     log.Verbose("- {0} = ***", key);
                  else
                     log.Verbose("- {0} = {1}", key, value
                        .Replace("\\", "\\\\")
                        .ReplaceX("\\r?\\n", "\\n")
                     );
               });
         }
         finally
         {
            cfg = null!; // cleanup
         }
      }

      private void AddSource(MyFileProvider provider, Func<string> fname)
      {
         var src = new MySource(() => ResolveConfigurationProvider(provider, fname()));
         AddSource(src);
      }

      internal void AddSource(MyFileProvider provider, string name, Func<IEnumerable<string>> suffixes)
      {
         var src = new MyMultiSource(() =>
         {
            var list = new List<IConfigurationSource>();
            suffixes().ForEach(x =>
               list.Add(new MySource(() =>
                  ResolveConfigurationProvider(provider, $"{name}.{x}.yaml"))));
            return list;
         });
         AddSource(src);
      }

      private void AddSource(IMySource src)
      {
         sources.Add(src);
         Snapshot();
      }

      private void AddSource(Func<IConfigurationProvider> provider) => AddSource(new MySource(provider));

      private void AddEnvSource()
      {
         var src = new MyMultiSource(() =>
         {
            var list = new List<IConfigurationSource>();
            EnvironmentPrefix.ForEach(x =>
               list.Add(new MySource(() =>
                  ResolveEnvConfigurationProvider(x))));
            return list;
         });
         sources.Add(src);
         Snapshot();
      }

      private void AddCliSource() => AddSource(ResolveCliConfigurationProvider);


      private IConfigurationProvider ResolveConfigurationProvider(string id, Func<IConfigurationProvider> provider)
      {
         providerIndex.TryGetValue(id, out var p);
         if (p != null) return p;

         p = providerIndex[id] = provider();
         p.Load();

         return p;
      }

      private IConfigurationProvider ResolveConfigurationProvider(MyFileProvider fileProvider, string fileName)
      {
         var id = fileProvider.IsEmbedded
            ? $"[embedded] {fileName}"
            : $"[file] {fileProvider.Root}/{fileName}";
         return ResolveConfigurationProvider(id, () => new YamlCfgProvider(new YamlConfigurationSource
         {
            FileProvider = fileProvider,
            Path = fileName,
            Optional = true,
            ReloadOnChange = !fileProvider.IsEmbedded,
            OnLoadException = OnLoadException,
         }));
      }

      internal static void OnLoadException(FileLoadExceptionContext ctx)
      {
         var msg = ctx.Provider.Source.FileProvider.GetFileInfo(ctx.Provider.Source.Path).PhysicalPath
            ?? $"[embedded] {ctx.Provider.Source.Path}";
         if (ctx.Exception.Message.Contains("Unable to cast"))
            msg = $"{msg} is probably empty, provide at least one element";
         throw new ConfigurationException(msg, ctx.Exception);
      }

      private IConfigurationProvider ResolveEnvConfigurationProvider(string environmentPrefix)
      {
         return ResolveConfigurationProvider(
            $"[env:{environmentPrefix}]",
            () => new MyEnvConfigurationProvider(environmentPrefix));
      }

      private IConfigurationProvider ResolveCliConfigurationProvider()
      {
         return ResolveConfigurationProvider(
            "[cli]",
            () => new MyCliConfigurationProvider(CliConfigurationProvider)
         );
      }

      private IConfigurationBuilder Populate(IConfigurationBuilder builder)
      {
         sources.ForEach(x => builder.AddMultiSource(x));
         builder.AddDecryptor();
         builder.AddResolver();
         //builder.Add(new MySource(() => new WritableConfigurationProvider())); // writable layer on top
         return builder;
      }

      private void Snapshot()
      {
         var builder = new ConfigurationBuilder();
         sources.ForEach(x =>
         {
            var stage = new ConfigurationBuilder().AddMultiSource(x).Build();
            builder.AddInMemoryCollection(stage.AsEnumerable());
         });

         cfg = builder.Build();
         Environment = cfg.GetConfigurationProperty(Option.Environment) ?? Environment;
         Includes = sources.BuildIncrementalList(CfgOption.Includes);
         Profiles = sources.BuildIncrementalList(CfgOption.Profiles);
      }

   }
}
