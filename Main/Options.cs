using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using JRevolt.YamlSecrets;
using Microsoft.Extensions.Configuration;
using Serilog;

namespace JRevolt.Configuration
{
   // root cfg options
   public enum Option
   {
      // defined by [Web]HostDefaults
      Environment,
      ApplicationName,
      Urls,

      // jrevolt
      Configuration,
   }

   // "Configuration" node
   public enum CfgOption
   {
      // log/debug
      Debug,
      Quiet,

      // locations
      ApplicationDirectory,
      EnvironmentPrefix,

      // structure
      Includes,
      Profiles,

      // secrets
      PrivateKey,
      MaskSecrets,

      // resolver
      ResolveReferences, // true|false
   }

   public static class CfgExtensions
   {
      public static string PropertyName(this CfgOption option) => $"{Option.Configuration}:{option}";

      public static string PropertyName(this Option option) => option.ToString();

      public static string? GetConfigurationProperty(this IConfiguration cfg, Option option) => cfg[option.PropertyName()];

      public static string? GetConfigurationProperty(this IConfiguration cfg, CfgOption option) => cfg[option.PropertyName()];


      /// <summary>
      /// This will add another cfg source layer which overrides encrypted cfg items with their decrypted values.
      /// </summary>
      /// <param name="builder"></param>
      /// <returns></returns>
      public static IConfigurationBuilder AddDecryptor(this IConfigurationBuilder builder)
      {
         var property = CfgOption.PrivateKey.PropertyName();

         // need configuration as known at this stage, to access private key needed for decryption
         var tmp = builder.Build();
         // here is the key
         var key = tmp[property];

         if (key is null)
         {
            Log.Debug("Secrets decryption is disabled. Enable by providing {0}=<file path or key content>", property);
            return builder;
         }

         Log.Debug("Enabling secrets decryption. Disable by resetting {0}", property);

         // prepare decryption utility
         var secrets = new Secrets(key);

         // now, encrypted data is tagged using !<encrypted> YAML tag, which would be lost during YAML parsing and conversion
         // to a key/value strings.
         // We rely on custom hooks to parser which persist the YAML tag as special string prefix of still encrypted value.
         var prefix = YamlCfgParser.EncryptedPrefixInline;
         // now we need to find all values with this special prefix, strip the prefix, and run value through decryption process
         var data = tmp.AsEnumerable()
               .Where(x => x.Value?.StartsWith(prefix) ?? false)
               .Select(x => KeyValuePair.Create(x.Key, x.Value[prefix.Length..]))
               .Select(x => x.DecryptValue(secrets))
               .ToDictionary(x => x.Key, x => x.Value)
            ;
         return builder.Add(new MySource(() => new DecryptionConfigurationProvider(data)));
      }

      public static IConfigurationBuilder AddResolver(this IConfigurationBuilder builder)
      {
         var property = CfgOption.ResolveReferences.PropertyName();

         var tmp = builder.Build();

         bool.TryParse(tmp[property] ?? "false", out var enabled);

         if (!enabled)
         {
            Log.Debug("References' Resolver is disabled. Enable using {0}=true", property);
            return builder;
         }

         Log.Debug("References Resolver enabled. Disable using {0}=false", property);

         var data = tmp.AsEnumerable()
            .Where(x => x.Value != null && Regex.IsMatch(x.Value, "\\$\\{.*\\}"))
            .Select(x => new KeyValuePair<string, string>(x.Key, x.Value.ResolveReferences(tmp)))
            .ToDictionary(x => x.Key, x => x.Value)
            ;

         return builder.Add(new MySource(() => new ResolverConfigurationProvider(data)));
      }

   }

}
