using System;
using System.Linq;
using JetBrains.Annotations;
using Microsoft.Extensions.Primitives;

namespace JRevolt.Configuration.CommandLine
{
   [AttributeUsage(AttributeTargets.Class), MeansImplicitUse]
   public class CliCommandAttribute : Attribute
   {
      public CliCommandAttribute(string? name = null)
      {
         var aliases = name?.Split(",");
         Name = aliases?.First();
         Aliases = aliases?.ToArray();
      }

      public string? Name { get; set; }

      public string[]? Aliases { get; }


      public string? Description { get; init; }
      public bool Root { get; set; }

   }

   [AttributeUsage(AttributeTargets.Property), MeansImplicitUse]
   public class CliOptionAttribute : Attribute
   {
      public CliOptionAttribute(string? forms = null)
      {
         if (forms == null) return;
         var tokens = forms.Split(",")
            .Select(s => s.Trim())
            .ToList();
         Long = tokens.Where(x => x.StartsWith("--") && x.Length>2).ToArray();
         Short = tokens.Where(x => x.StartsWith("-") && !x.StartsWith("--") && x.Length>1).ToArray();
      }

      public string[] Short { get; } = StringValues.Empty;
      public string[] Long { get; } = StringValues.Empty;
      public string? Description { get; init; }
   }

   [AttributeUsage(AttributeTargets.Method), MeansImplicitUse]
   public class CliEntrypointAttribute : Attribute
   {
   }

   [AttributeUsage(AttributeTargets.Property), MeansImplicitUse]
   public class CliRemainderAttribute : Attribute
   {
   }

   [AttributeUsage(AttributeTargets.Class|AttributeTargets.Property)]
   public class CliDescriptionAttribute : Attribute
   {
      public string Description { get; }

      public CliDescriptionAttribute(string description) => Description = description;
   }

   public interface ICliConverter
   {
      Type Source { get; }
      Type Target { get; }
      object? Convert(object? src);
   }

   public abstract class CliConverter<T,TR> : ICliConverter
   {
      public Type Source { get; } = typeof(T);
      public Type Target { get; } = typeof(TR);

      protected abstract TR? Convert(T? src);

      public object? Convert(object? src) => Convert((T) src!);
   }

   public delegate object? CliCommandFactory(Type type);

}
