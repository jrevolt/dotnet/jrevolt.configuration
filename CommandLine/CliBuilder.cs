using System;
using System.CommandLine;
using System.Linq;
using System.Reflection;
using Serilog;

namespace JRevolt.Configuration.CommandLine
{
   public class CliBuilder
   {
      public static CliBuilder Create<T>() => new CliBuilder(typeof(T));

      private readonly Assembly[] assemblies;

      public CliBuilder(params Assembly[] assemblies) => this.assemblies = assemblies;
      public CliBuilder(Type program) => assemblies = new[]{program.Assembly};

      private ILogger Log => Serilog.Log.ForContext(GetType());

      private CliCommandFactory Factory { get; set; } = Activator.CreateInstance;

      internal Func<Type, bool> TypeFilter { get; init; } = _ => true;

      public CliBuilder WithCliCommandFactory(CliCommandFactory factory)
      {
         Factory = factory;
         return this;
      }

      public Command Build()
      {
         var index = assemblies
            .SelectMany(x => x.GetTypes())
            .Where(t => t.GetCustomAttribute<CliCommandAttribute>() != null)
            .Where(TypeFilter)
            .ToDictionary(t => t, t => new CliCommand(t, Factory));

         // find root
         var roots = index
            .Where(x => x.Key.GetCustomAttribute<CliCommandAttribute>()!.Root)
            .Select(x => x.Value)
            .ToList();

         var root = new RootCommand() as Command;
         switch (roots.Count)
         {
            case 1:
               root = roots.First();
               Log.Debug("Command {0} elected as root.", root.Name);
               break;
            case > 1:
               Log.Warning("Multiple root commands found. Packing them under single root. {0}", roots.Select(x => x.Name));
               root = new RootCommand();
               break;
         }

         // fixup command hierarchy
         index
            .Where(x => x.Value != root)
            .ToList().ForEach(x =>
            {
               var (key, value) = x;
               var parentType = key.BaseType ?? typeof(object);
               index.TryGetValue(parentType, out var parent);
               (parent ?? root).AddCommand(value);
            });

         // fixup root name
         if (root is not RootCommand) root.Name = new RootCommand().Name;

         return root;
      }
   }

}
