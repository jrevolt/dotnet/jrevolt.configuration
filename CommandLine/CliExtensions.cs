using System.Collections.Generic;
using System.CommandLine.Parsing;
using System.Linq;
using System.Reflection;
using JetBrains.Annotations;

namespace JRevolt.Configuration.CommandLine
{
   internal static class InternalExtensions
   {
      internal static void SetProperty(this object dst, PropertyInfo property, object value)
      {
         if (property.CanWrite)
         {
            property.SetValue(dst, value);
         }
         else
         {
            property.DeclaringType!.GetRuntimeFields()
               .First(f => f.Name.Contains($"<{property.Name}>"))
               .SetValue(dst, value);
         }
      }

      internal static IEnumerable<CommandResult> Ancestors(this CommandResult? current)
      {
         while (current != null)
         {
            yield return current;
            current = current.Parent as CommandResult;
         }
      }

   }

   [UsedImplicitly]
   internal class DictionaryConverter : CliConverter<string[], IDictionary<string,string>>
   {
      protected override IDictionary<string, string>? Convert(string[]? src) =>
         src?.Select(s => s.Split("=")).ToDictionary(items => items[0], items => items[1]);
   }

}
