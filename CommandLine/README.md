
# JRevolt.Configuration.CommandLine

## Quick Start

```c#
internal class Program {
  static internal void Main(string[] args) =>
	CliSupport.BuildCli(Assembly.GetExecutingAssembly()).Invoke(args);
}

[CliCommand]
internal class ListFilesCommand
{
  [CliOption]
  public bool All { get; }

  [CliEntrypoint]
  internal void Run() {
  	Console.WriteLine($"{GetType().FullName} = {JsonSerializer.Serialize(this)}");
  }
}

```

```sh
app.exe list-files --all
```
