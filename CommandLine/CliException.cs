using System;
using System.Diagnostics.CodeAnalysis;
using System.Runtime.Serialization;

namespace JRevolt.Configuration.CommandLine
{
   [Serializable]
   public class CliException : ApplicationException
   {
      public CliException(string? message = null, Exception? innerException = null) : base(message, innerException)
      {
      }
   }
}
