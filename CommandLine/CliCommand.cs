using System;
using System.Collections.Generic;
using System.CommandLine;
using System.CommandLine.Invocation;
using System.CommandLine.IO;
using System.CommandLine.Parsing;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;
using Serilog;

namespace JRevolt.Configuration.CommandLine
{
   internal class CliCommand : Command, ICommandHandler
   {
      private const BindingFlags OptionFlags = BindingFlags.Instance | BindingFlags.Public; //NOSONAR
      private const BindingFlags MethodFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic; //NOSONAR

      private ILogger Log => Serilog.Log.ForContext(GetType());

      private static string GetName(Type type)
      {
         var attr = type.GetCustomAttribute<CliCommandAttribute>()!;
         if (attr.Name != null) return attr.Name;
         attr.Name = Regex.Replace(type.Name, "Command$", "").ToKebabCase();
         return attr.Name;
      }

      private Type Type { get; }

      private CliCommandFactory Factory { get; }


      public CliCommand(Type type, CliCommandFactory factory) : base(GetName(type))
      {
         var attr = type.GetCustomAttribute<CliCommandAttribute>();

         attr?.Aliases?.ToList().ForEach(AddAlias);

         Description = attr?.Description ?? type.GetCustomAttribute<CliDescriptionAttribute>()?.Description;

         Type = type;
         Factory = factory;

         Log.Verbose("CLI ({0}): {1} : {2} => {3}", "cmd", Type.Name, Type.BaseType?.Name, Aliases);

         Type
            .GetProperties(OptionFlags)
            .Where(p => p.GetCustomAttribute<CliOptionAttribute>() != null)
            .Where(p => p.DeclaringType == Type || p.DeclaringType!.GetCustomAttribute<CliCommandAttribute>() is null)
            .ToList()
            .ForEach(p =>
            {
               var opt = p.GetCustomAttribute<CliOptionAttribute>();
               Debug.Assert(opt != null);
               var aliases = new List<string>();
               aliases.AddRange(opt.Short ?? StringValues.Empty);
               aliases.AddRange(opt.Long ?? StringValues.Empty);
               if (aliases.Count == 0) aliases.Add($"--{p.Name.ToKebabCase()}");
               // AddOption(new Option(
               //    aliases.ToArray(),
               //    opt.Description ?? p.GetCustomAttribute<CliDescriptionAttribute>()?.Description,
               //    p.PropertyType,
               //
               //    ));
               AddOption(new Option(aliases.ToArray(), argumentType: p.PropertyType)
               {
                  Description = opt.Description ?? p.GetCustomAttribute<CliDescriptionAttribute>()?.Description,
                  //Argument = new Argument{ArgumentType = p.PropertyType },
                  IsRequired = p.GetCustomAttribute<RequiredAttribute>() != null,
                  ArgumentHelpName = p.Name,
               });

               Log.Verbose("CLI ({0}): {1}.{2} : {3} => {4}", "opt", Type.Name, p.Name, p.PropertyType.Name, aliases);
            })
            ;
         Handler = this;
      }

      public async Task<int> InvokeAsync(InvocationContext context)
      {
         try
         {
            return await InvokeAsync0(context);
         }
         catch (Exception e)
         {
            context.ExitCode = 1;
            context.InvocationResult = new ExceptionInvocationResult(new ApplicationException(null, e));
            return context.ExitCode;
         }
      }

      private async Task<int> InvokeAsync0(InvocationContext context)
      {
         Log.Verbose("Preparing command {0} : {1}", Type.Name, context.ParseResult);

         var bind = context.BindingContext;

         var action = Factory.Invoke(Type) ?? throw new CliException();

         Type.GetProperties(OptionFlags)
            .Where(p => p.GetCustomAttribute<CliOptionAttribute>() != null)
            .ToList()
            .ForEach(p =>
            {
               var attr = p.GetCustomAttribute<CliOptionAttribute>();
               Debug.Assert(attr != null);
               var alias = attr.Long.FirstOrDefault() ?? attr.Short.FirstOrDefault() ?? $"--{p.Name.ToKebabCase()}";
               // find options
               var opt = bind.ParseResult.CommandResult.Ancestors()
                     .Reverse()
                     .Select(c => c.Command.Options.FirstOrDefault(o => o.HasAlias(alias)))
                     .First(o => o != null)!;

               // find value
               var value = bind.ParseResult.FindResultFor(opt)?.GetValueOrDefault();
               if (value == null) return;

               bool IsConversionNeeded() => !p.PropertyType.IsInstanceOfType(value);
               bool UseChangeType() => p.PropertyType.IsValueType || typeof(IConvertible).IsAssignableFrom(p.PropertyType);

               if (IsConversionNeeded() && UseChangeType())
                  value = Convert.ChangeType(value, p.PropertyType);

               if (IsConversionNeeded())
               {
                  Debug.Assert(p.DeclaringType != null);
                  var type = value.GetType();
                  value = new []{GetType().Assembly, p.DeclaringType.Assembly}
                        // get all types
                        .SelectMany( x => x.GetTypes())
                        // filter instantiable converter classes
                        .Where(x => typeof(ICliConverter).IsAssignableFrom(x) && x.IsClass && !x.IsAbstract)
                        // create instance
                        .Select(x => (ICliConverter) Activator.CreateInstance(x)!)
                        // check if it's compatible
                        .Where(x => x.Target == p.PropertyType && x.Source == type)
                        // use it to convert value
                        .Select(x => x.Convert(value))
                        .FirstOrDefault()
                     ;
               }

               Log.Verbose("Set option {0}.{1} : {2} = {3}", Type.Name, p.Name, p.PropertyType.Name, value);

               action.SetProperty(p, value!);
            });

         // inject unparsed remainder of the command line, if needed
         if (bind.ParseResult.UnparsedTokens.Count > 0)
         {
            Type.GetProperties(OptionFlags)
               .Where(p => p.GetCustomAttribute<CliRemainderAttribute>() != null)
               .ToList()
               .ForEach(p =>
               {
                  var value = bind.ParseResult.UnparsedTokens.ToArray();
                  action.SetProperty(p, value);
               });
         }

         var vctx = new ValidationContext(action);
         var vresult = new List<ValidationResult>();
         var valid = Validator.TryValidateObject(action, vctx, vresult, true);
         if (!valid)
         {
            vresult.ToList().ForEach(x => context.Console.Error.WriteLine(x.ToString()));
            context.Console.Error.WriteLine($"Validation failed: {context.ParseResult}");
            return await Task.FromResult(1);
         }

         var method = action //NOSONAR
            .GetType()
            .GetMethods(MethodFlags)
            .Where(x => x.DeclaringType == action.GetType())
            .Single(m => m.GetCustomAttribute<CliEntrypointAttribute>() != null);

         async Task<int> Invocation()
         {
            var result = method.Invoke(action, null);
            if (result is Task task)
            {
               await task;
               var isVoid = method.ReturnType.GetGenericArguments().Length == 0;
               result = isVoid ? null : (task as dynamic).GetAwaiter().GetResult();
            }
            return result is int i ? i : 0;
         }

         Log.Debug("Executing {0}.{1} : {2}", Type.Name, method.Name, action);

         return await Invocation();
      }

   }

   internal class ExceptionInvocationResult : IInvocationResult
   {
      private Exception Exception { get; }

      public ExceptionInvocationResult(Exception exception) => Exception = exception;

      public void Apply(InvocationContext context) => throw Exception;
   }
}
